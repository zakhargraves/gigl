import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { DragScrollModule } from 'ngx-drag-scroll';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    DragScrollModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
