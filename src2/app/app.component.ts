import { Component } from '@angular/core';
import { NgClass } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
	homeMode: boolean;
  cardHover: boolean;
  public picker : number = 1;



  constructor(){
  	this.homeMode = true;

  }

  homeTrigger(){
  	if(this.homeMode){
  		this.homeMode = false;
  	}else{
  		this.homeMode = true;
  	}
  	console.log("Home trigger " + this.homeMode);
  }

  homeLogo(){
  	if(this.homeMode){
		return {
			'smalllogo' : true
		}
  	}
  }

  backgroundControl() {
  	if(this.homeMode){
  		return {
  			'MainBackground' : true,
  			'LoginBackground' : false
  		}
  	} else {
  		return {
  			'MainBackground' : false,
  			'LoginBackground' : true
  		}
  	}
  }

  homeFbHideBtn(){
  	if(this.homeMode){
  		return {
  			'hiddenbutton' : true
  		}
  	}
  }

hidebox(){
  if(!this.homeMode){
    return {
      'HideGrid' : true
    }
  }

}


 HeaderCard(){
  	if(this.homeMode){
  		return {
  			'HeaderCard' : true
  		}
  	}
  }



  shiftBackground(){
    this.picker += 1;
  }

  changeStyle($event){
    if(this.cardHover){
      this.cardHover = false;
    }else{
      this.cardHover = true;
    }
  }

  shiftBackgroundStyle(){

    console.log(this.picker)

    if(this.picker > 11){
        this.picker = 1;
    }

    if(this.picker == 1){
        return "url('https://images.unsplash.com/photo-1506744038136-46273834b3fb?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=02ea02a05656a91ed66f0ea7e12e1ded&auto=format&fit=crop&w=1350&q=80')"
    }else if(this.picker == 2){
        return "url('https://images.unsplash.com/reserve/unsplash_52d8277ccad75_1.JPG?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=2af119a433c4c4f966bbff9d15dbb810&auto=format&fit=crop&w=1350&q=80')"
    }else if(this.picker == 3){
        return "url('https://i.pinimg.com/564x/5c/94/ef/5c94ef2e75e5de47f849f98637aa9a63.jpg')"
    }else if(this.picker == 4){
        return "url('https://i.pinimg.com/564x/55/00/10/5500107d2f17c06f225b98b5bb033731.jpg')"
    }else if(this.picker == 5){
        return "url('https://images.unsplash.com/photo-1504288145234-919e7bbc6d19?ixlib=rb-0.3.5&s=d4c6e5eb2e1fcf491b2f1477236c8977&auto=format&fit=crop&w=1500&q=80')"
    }else if(this.picker == 6){
        return "url('https://images.unsplash.com/photo-1505370914932-3d5df10b0d4e?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=21859dab6faa592288a37c7b466f936e&auto=format&fit=crop&w=1498&q=80')"

    }else if(this.picker == 7){
      return "url('https://images.unsplash.com/photo-1486546910464-ec8e45c4a137?ixlib=rb-0.3.5&s=1e2034a7663dc7b378e3b1707acc9653&auto=format&fit=crop&w=951&q=80')"
    }else if(this.picker == 8){
      return "url('https://images.unsplash.com/photo-1484903820457-9e585d707e9d?ixlib=rb-0.3.5&s=37944772e0f1c62912a479264ddf2759&auto=format&fit=crop&w=931&q=80')"
    }else if(this.picker == 9){
      return "url('https://images.unsplash.com/photo-1485163819542-13adeb5e0068?ixlib=rb-0.3.5&s=9d11f8fe830175409a91bb037f148c2d&auto=format&fit=crop&w=934&q=80')"
    }else if(this.picker == 10){
      return "url('https://images.unsplash.com/photo-1518489979617-2693e50ddf3a?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8f5018dbd16dd41e59dd68da0283bd6b&auto=format&fit=crop&w=1950&q=80')"
    }else if(this.picker == 11){
      return "url('https://images.unsplash.com/photo-1417577097439-425fb7dec05e?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=93a5dd474dcb87271705bb8a7edf8a98&auto=format&fit=crop&w=1489&q=80')"
  }


  }






}
