import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'app';
  loggedIn: boolean = true;
  playMode: number = 0;
  playing: number = 0;
  liked: number = 0;

    constructor() {



    }

    activatePlay(id){
      this.playMode = id;
    }

    unactivatePlay(){
      this.playMode = 0;
    }

    isPlaying(id){
      this.playing = id;
    }

    isNotPlaying(){
      this.playing = 0;
    }

    fullHeart(id){
      console.log(id)
      this.liked = id
    }

    unfullHeart(){
      this.liked = 0
    }

}
